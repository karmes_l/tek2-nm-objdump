/*
** getSh_Section.c for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Memoire/PSU_2015_nmobjdump
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed Feb 24 19:05:58 2016 Karmes Lionel
** Last update Sun Feb 28 00:04:47 2016 Karmes Lionel
*/

#include "nm.h"

Elf32_Shdr	*getSh_Section32(Elf32_Ehdr *data, char *name,
				 uint32_t sh_type)
{
  Elf32_Shdr*	shdr;
  Elf32_Shdr	shdr_shstrtab;
  char		*string;
  int		i;

  shdr = (Elf32_Shdr*)((void*)data + data->e_shoff);
  shdr_shstrtab = shdr[data->e_shstrndx];
  string = (char*)((void*)data + shdr_shstrtab.sh_offset);
  i = 0;
  while (i < data->e_shnum)
    {
      if (shdr[i].sh_type != SHT_NULL)
	{
	  if (name && shdr[i].sh_type != SHT_NULL)
	    {
	      if (!strcmp(name, string + shdr[i].sh_name))
		return (&shdr[i]);
	    }
	  else
	    if (shdr[i].sh_type == sh_type)
	  return (&shdr[i]);
	}
    ++i;
    }
  return (NULL);
}

Elf64_Shdr	*getSh_Section64(Elf64_Ehdr *data, char *name,
				 uint32_t sh_type)
{
  Elf64_Shdr*	shdr;
  Elf64_Shdr	shdr_shstrtab;
  char		*string;
  int		i;

  shdr = (Elf64_Shdr*)((void*)data + data->e_shoff);
  shdr_shstrtab = shdr[data->e_shstrndx];
  string = (char*)((void*)data + shdr_shstrtab.sh_offset);
  i = 0;
  while (i < data->e_shnum)
    {
      if (shdr[i].sh_type != SHT_NULL)
	{
	  if (name)
	    {
	      if (!strcmp(name, string + shdr[i].sh_name))
		return (&shdr[i]);
	    }
	  else
	    if (shdr[i].sh_type == sh_type)
	  return (&shdr[i]);
	}
      ++i;
    }
  return (NULL);
}
