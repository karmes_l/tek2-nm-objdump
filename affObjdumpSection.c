/*
** affObjdumpSection.c for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Memoire/PSU_2015_nmobjdump
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Feb 23 19:42:14 2016 Karmes Lionel
** Last update Sun Feb 28 00:06:56 2016 Karmes Lionel
*/

#include "objdump.h"

void	printLineSection(char *deb, int end)
{
  int	i;

  printf("  ");
  i = 0;
  while (i < end)
    {
      if (isprint(*(deb + i)))
	printf("%c", *(deb + i));
      else
	printf(".");
      ++i;
    }
  while (i++ < 16)
    printf(" ");
  printf("\n");
}

void	affContentSection32(Elf32_Ehdr *data, Elf32_Shdr shdr)
{
  uint32_t	i;
  char		*deb;
  int		c;

  i = 0;
  deb = (char*)((void*)data + shdr.sh_offset);
  while (i < shdr.sh_size)
    {
      if (i % 16 == 0)
	{
	  if (i > 0)
	    printLineSection(deb + i - 16, 16);
	  printf(" %x ", (Elf32_Addr)shdr.sh_addr + i);
	}
      else if (i % 4 == 0)
	printf(" ");
      printf("%02hhx", *(deb + i));
      ++i;
    }
  c = i % 16;
  while (c % 16 != 0)
    printf("%s", (c++ % 4 == 0) ? "   " : "  ");
  printLineSection(deb + i - ((i % 16 == 0) ? 16 : i % 16),
		   (i % 16 == 0) ? 16 : i % 16);
}

void		affContentSection64(Elf64_Ehdr *data, Elf64_Shdr shdr)
{
  uint64_t	i;
  char		*deb;
  int		c;

  i = 0;
  deb = (char*)((void*)data + shdr.sh_offset);
  while (i < shdr.sh_size)
    {
      if (i % 16 == 0)
	{
	  if (i > 0)
	    printLineSection(deb + i - 16, 16);
	  printf(" %lx ", (Elf64_Addr)shdr.sh_addr + i);
	}
      else if (i % 4 == 0)
	printf(" ");
      printf("%02hhx", *(deb + i));
      ++i;
    }
  c = i % 16;
  while (c % 16 != 0)
    printf("%s", (c++ % 4 == 0) ? "   " : "  ");
  printLineSection(deb + i - ((i % 16 == 0) ? 16 : i % 16),
		   (i % 16 == 0) ? 16 : i % 16);
}

void		affShdr32(Elf32_Ehdr *data)
{
  Elf32_Shdr*	shdr;
  Elf32_Shdr	shdr_shstrtab;
  char		*string;
  int		i;

  shdr = (Elf32_Shdr*)((void*)data + data->e_shoff);
  shdr_shstrtab = shdr[data->e_shstrndx];
  string = (char*)((void*)data + shdr_shstrtab.sh_offset);
  i = 0;
  while (i < data->e_shnum) {
    if (shdr[i].sh_type != SHT_NULL && shdr[i].sh_size > 0
	&& shdr[i].sh_type != SHT_NOBITS) {
      printf("Contents of section %s:\n", string + shdr[i].sh_name);
      affContentSection32(data, shdr[i]);
    }
    ++i;
  }
}

void		affShdr64(Elf64_Ehdr *data)
{
  Elf64_Shdr*	shdr;
  Elf64_Shdr	shdr_shstrtab;
  char		*string;
  int		i;

  shdr = (Elf64_Shdr*)((void*)data + data->e_shoff);
  shdr_shstrtab = shdr[data->e_shstrndx];
  string = (char*)((void*)data + shdr_shstrtab.sh_offset);
  i = 0;
  while (i < data->e_shnum) {
    if (shdr[i].sh_type != SHT_NULL
	&& shdr[i].sh_size > 0 && shdr[i].sh_type != SHT_NOBITS)
      {
	printf("Contents of section %s:\n", string + shdr[i].sh_name);
	affContentSection64(data, shdr[i]);
      }
    ++i;
  }
}
