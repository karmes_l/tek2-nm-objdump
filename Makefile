##
## Makefile for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Memoire/PSU_2015_nmobjdump
## 
## Made by Karmes Lionel
## Login   <karmes_l@epitech.net>
## 
## Started on  Tue Feb 23 11:46:37 2016 Karmes Lionel
## Last update Sun Feb 28 16:49:36 2016 Karmes Lionel
##

CC	=	gcc

RM	=	rm -f

CFLAGS	+=	-Wextra -Wall -Werror
CFLAGS	+=	-I./include/

LDFLAGS	=

SRCS1	=	nm.c \
		affNm.c \
		loadSymbols.c \
		getSh_Section.c

RULE1	=	nm

NAME1	=	my_nm

OBJS1	=	$(SRCS1:.c=.o)

SRCS2	=	objdump.c \
		affObjdump.c \
		affObjdumpSection.c

RULE2	=	objdump

NAME2	=	my_objdump

OBJS2	=	$(SRCS2:.c=.o)


all:		$(RULE1) $(RULE2)

$(RULE1):	$(NAME1)

$(RULE2):	$(NAME2)

$(NAME1):	$(OBJS1)
		$(CC) $(OBJS1) -o $(NAME1)

$(NAME2):	$(OBJS2)
		$(CC) $(OBJS2) -o $(NAME2)
clean:
		$(RM) $(OBJS1)
		$(RM) $(OBJS2)

fclean:		clean
		$(RM) $(NAME1)
		$(RM) $(NAME2)

re:		fclean all

.PHONY:		all $(RULE1) $(RULE2) clean fclean re
