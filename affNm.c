/*
** affNm.c for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Memoire/PSU_2015_nmobjdump
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed Feb 24 18:31:32 2016 Karmes Lionel
** Last update Sun Feb 28 16:40:24 2016 Karmes Lionel
*/

#include "nm.h"

int	sortSymbolByName(const void *a, const void *b)
{
  char	*name_a;
  char	*name_b;

  name_a = ((t_symbol*)a)->name;
  while (name_a && *name_a != '\0' && *name_a == '_')
    ++name_a;
  name_b = ((t_symbol*)b)->name;
  while (name_b && *name_b != '\0' && *name_b == '_')
    ++name_b;
  return (strcasecmp(name_a, name_b));
}

void		affSh_Symbols32(t_symbol *symbols, uint64_t size)
{
  uint64_t	i;

  i = 0;
  while (i < size)
    {
      if (symbols[i].symbol->st_name
	  && ELF64_ST_TYPE(symbols[i].symbol->st_info) != STT_FILE )
	{
	  if (symbols[i].symbol->st_value)
	    printf("%08x %s\n", (Elf32_Addr)symbols[i].symbol->st_value,
		   symbols[i].name);
	  else
	    printf("%8s %s\n", "", symbols[i].name);
	}
      ++i;
    }
}

void		affSh_Symbols64(t_symbol *symbols, uint64_t size)
{
  uint64_t	i;

  i = 0;
  while (i < size)
    {
      if (symbols[i].symbol->st_name
	  && ELF64_ST_TYPE(symbols[i].symbol->st_info) != STT_FILE )
	{
	  if (symbols[i].symbol->st_value)
	    printf("%016lx %s\n", (Elf64_Addr)symbols[i].symbol->st_value,
		   symbols[i].name);
	  else
	    printf("%16s %s\n", "", symbols[i].name);
	}
      ++i;
    }
}

void		affNm32(Elf32_Ehdr *data, const char *filename)
{
  t_symbol	*symbols;
  uint32_t	size;

  if ((data->e_type != ET_EXEC && data->e_shoff == 0)
      || data->e_shstrndx == SHN_UNDEF)
    {
      fprintf(stderr, "/usr/bin/nm: %s: File format not recognized\n",
	      filename);
      return ;
    }
  if (!(symbols = loadSymbols32(data, &size)))
    {
      fprintf(stderr, "/usr/bin/nm: %s: no symbols\n", filename);
      return ;
    }
  qsort(symbols, size, sizeof(t_symbol), &sortSymbolByName);
  affSh_Symbols32(symbols, size);
  free(symbols[0].symbol);
  free(symbols);
}

void		affNm64(Elf64_Ehdr *data, const char *filename)
{
  t_symbol	*symbols;
  uint64_t	size;

  if ((data->e_type != ET_EXEC && data->e_shoff == 0)
      || data->e_shstrndx == SHN_UNDEF)
    {
      fprintf(stderr, "/usr/bin/nm: %s: File format not recognized\n",
	      filename);
      return ;
    }
  if (!(symbols = loadSymbols64(data, &size)))
    {
      fprintf(stderr, "/usr/bin/nm: %s: no symbols\n", filename);
      return ;
    }
  qsort(symbols, size, sizeof(t_symbol), &sortSymbolByName);
  affSh_Symbols64(symbols, size);
  free(symbols[0].symbol);
  free(symbols);
}
