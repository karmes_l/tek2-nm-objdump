/*
** my_objdump.c for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Memoire/PSU_2015_nmobjdump
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Feb 23 11:56:35 2016 Karmes Lionel
** Last update Sun Feb 28 00:30:44 2016 Karmes Lionel
*/

#include "objdump.h"

unsigned char	checkClass(Elf32_Ehdr* data)
{
  if (data->e_ident[EI_MAG0] != ELFMAG0
      || data->e_ident[EI_MAG1] != ELFMAG1
      || data->e_ident[EI_MAG2] != ELFMAG2
      || data->e_ident[EI_MAG3] != ELFMAG3)
    return (ELFCLASSNONE);
  return (data->e_ident[EI_CLASS]);
}

void		objdump_next(void *data, const char *filename)
{
  int		class;

  if ((class = checkClass((Elf32_Ehdr*)data)) == ELFCLASSNONE)
    fprintf(stderr, "/usr/bin/nm: %s: File format not recognized\n", filename);
  else if (class == ELFCLASS32)
    affObjdump32((Elf32_Ehdr*)data, filename);
  else
    affObjdump64((Elf64_Ehdr*)data, filename);
}

void		objdump(const char *filename)
{
  int		fd;
  struct stat	file_infos;
  void		*data;

  if ((fd = open(filename, O_RDONLY)) == -1)
    fprintf(stderr, "Not such file '%s' or Permission denied\n", filename);
  else
    {
      if (fstat(fd, &file_infos) == -1)
	fprintf(stderr, "Error fstat() on '%s'\n", filename);
      else if ((data = mmap(0, file_infos.st_size, PROT_READ, MAP_PRIVATE,
			    fd, 0)) == MAP_FAILED)
    fprintf(stderr, "Error mmap() on '%s'\n", filename);
      else
	{
	  objdump_next(data, filename);
	  munmap(data, file_infos.st_size);
	}
      close(fd);
    }
}

int	main(int ac, const char *av[])
{
  int	i;

  if (ac <= 1)
    objdump("a.out");
  else
    {
      i = 1;
      while (i < ac)
	objdump(av[i++]);
    }
  return  (0);
}
