/*
** loadSymbols.c for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Memoire/PSU_2015_nmobjdump
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed Feb 24 18:54:27 2016 Karmes Lionel
** Last update Sun Feb 28 00:43:33 2016 Karmes Lionel
*/

#include "nm.h"

t_symbol	*setSymbols32(Elf32_Sym *symbols, uint32_t size,
			      void *_strtabAddr)
{
  uint32_t	i;
  t_symbol	*my_symbols;

  if (!(my_symbols = malloc(sizeof(t_symbol) * size)))
    return (NULL);
  i = 0;
  while (i < size)
    {
      my_symbols[i].symbol = (Elf64_Sym*)&(symbols[i]);
      my_symbols[i].name = (char*)_strtabAddr + symbols[i].st_name;
      ++i;
    }
  return (my_symbols);
}

t_symbol	*setSymbols64(Elf64_Sym *symbols, uint64_t size,
			      void *_strtabAddr)
{
  uint64_t	i;
  t_symbol	*my_symbols;

  if (!(my_symbols = malloc(sizeof(t_symbol) * size)))
    return (NULL);
  i = 0;
  while (i < size)
    {
      my_symbols[i].symbol = &(symbols[i]);
      my_symbols[i].name = (char*)_strtabAddr + symbols[i].st_name;
      ++i;
    }
  return (my_symbols);
}

t_symbol	*loadSymbols32(Elf32_Ehdr *data, uint32_t *size)
{
  Elf32_Shdr	*_strtab;
  Elf32_Shdr	*_symtab;
  Elf32_Sym	*symbols;

  if (!(_symtab = getSh_Section32(data, NULL, SHT_SYMTAB))
      || !(_strtab = getSh_Section32(data, ".strtab", 0)))
    return (NULL);
  *size = _symtab->sh_size / _symtab->sh_entsize;
  if (!(symbols = malloc(sizeof(Elf32_Sym) * (*size))))
    {
      fprintf(stderr, "Malloc couldn't alloc memory");
      return (NULL);
    }
  memcpy(symbols, (void*)data + _symtab->sh_offset, _symtab->sh_size);
  return (setSymbols32(symbols, *size, (void*)data + _strtab->sh_offset));
}

t_symbol	*loadSymbols64(Elf64_Ehdr *data, uint64_t *size)
{
  Elf64_Shdr	*_strtab;
  Elf64_Shdr	*_symtab;
  Elf64_Sym	*symbols;

  if (!(_symtab = getSh_Section64(data, NULL, SHT_SYMTAB))
      || !(_strtab = getSh_Section64(data, ".strtab", 0)))
    return (NULL);
  *size = _symtab->sh_size / _symtab->sh_entsize;
  if (!(symbols = malloc(sizeof(Elf64_Sym) * (*size))))
    {
      fprintf(stderr, "Malloc couldn't alloc memory");
      return (NULL);
    }
  memcpy(symbols, (void*)data + _symtab->sh_offset, _symtab->sh_size);
  return (setSymbols64(symbols, *size, (void*)data + _strtab->sh_offset));
}
