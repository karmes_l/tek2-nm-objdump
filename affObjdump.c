/*
** affObjdump.c for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Memoire/PSU_2015_nmobjdump
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Feb 23 12:50:22 2016 Karmes Lionel
** Last update Sun Feb 28 00:45:44 2016 Karmes Lionel
*/

#include "objdump.h"

char	*getArchi(uint16_t machine)
{
  char	*archi;

  archi = Archi(machine, EM_X86_64, "x86_64");
  if (archi[0] == '\0')
    return ("unknow");
  return (archi);
}

void	affEhdr(Elf32_Ehdr *data, unsigned char class)
{
  char	*archi;

  archi = getArchi(data->e_machine);
  printf("-%s\n", archi);
  printf("architecture: i386:%s, flags %0#6x\n", archi, data->e_flags);
  if (class == ELFCLASS32)
    printf("start address %#10x\n", data->e_entry);
  else
    printf("start address %0#18lx\n", (Elf64_Addr)data->e_entry);
}

void	affObjdump32(Elf32_Ehdr *data, const char *filename)
{
  Elf32_Shdr*	shdr;
  Elf32_Shdr	shdr_shstrtab;

  shdr = (Elf32_Shdr*)((void*)data + data->e_shoff);
  shdr_shstrtab = shdr[data->e_shstrndx];
  if ((data->e_type != ET_EXEC && data->e_shoff == 0) ||
      data->e_shstrndx == SHN_UNDEF || shdr_shstrtab.sh_type == SHT_NULL)
    {
      fprintf(stderr, "/usr/bin/nm: %s: File format not recognized\n",
	      filename);
      return ;
    }
  printf("\n%s:     file format elf32", filename);
  affEhdr(data, ELFCLASS32);
  printf("\n");
  affShdr32(data);
}

void	affObjdump64(Elf64_Ehdr *data, const char *filename)
{
  Elf64_Shdr*	shdr;
  Elf64_Shdr	shdr_shstrtab;

  shdr = (Elf64_Shdr*)((void*)data + data->e_shoff);
  shdr_shstrtab = shdr[data->e_shstrndx];
  if ((data->e_type != ET_EXEC && data->e_shoff == 0) ||
      data->e_shstrndx == SHN_UNDEF || shdr_shstrtab.sh_type == SHT_NULL)
    {
      fprintf(stderr, "/usr/bin/nm: %s: File format not recognized\n",
	      filename);
      return ;
    }
  printf("\n%s:     file format elf64", filename);
  affEhdr((Elf32_Ehdr*)data, ELFCLASS64);
  printf("\n");
  affShdr64(data);
}
