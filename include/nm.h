/*
** nm.h for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Memoire/PSU_2015_nmobjdump
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed Feb 24 18:48:02 2016 Karmes Lionel
** Last update Wed Feb 24 23:08:36 2016 Karmes Lionel
*/

#ifndef NM_H_
# define NM_H_

# include <elf.h>
# include <stdio.h>
# include <fcntl.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <sys/mman.h>
# include <string.h>
# include <unistd.h>
# include <stdlib.h>

typedef struct	s_symbol
{
  char		*name;
  Elf64_Sym	*symbol;
}		t_symbol;

void		affNm32(Elf32_Ehdr *data, const char *filename);
void		affNm64(Elf64_Ehdr *data, const char *filename);
t_symbol	*loadSymbols32(Elf32_Ehdr *data, uint32_t *size);
t_symbol	*loadSymbols64(Elf64_Ehdr *data, uint64_t *size);
Elf32_Shdr	*getSh_Section32(Elf32_Ehdr *data, char *name,
				 uint32_t sh_type);
Elf64_Shdr	*getSh_Section64(Elf64_Ehdr *data, char *name,
				 uint32_t sh_type);

#endif /* !NM_H_ */
