/*
** objdump.h for  in /home/karmes_l/Projets/tek2/Systeme_Unix-Memoire/PSU_2015_nmobjdump/include
** 
** Made by Karmes Lionel
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Feb 23 12:02:41 2016 Karmes Lionel
** Last update Fri Feb 26 17:41:34 2016 Karmes Lionel
*/

#ifndef OBJDUMP_H_
# define OBJDUMP_H_

# include <elf.h>
# include <stdio.h>
# include <fcntl.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <sys/mman.h>
# include <string.h>
# include <unistd.h>
# include <ctype.h>

# define Archi(e_machine, x, str)  ((e_machine == x) ? str : "")

void	  affObjdump32(Elf32_Ehdr *data, const char *filename);
void	  affObjdump64(Elf64_Ehdr *data, const char *filename);
void	  affShdr32(Elf32_Ehdr *data);
void	  affShdr64(Elf64_Ehdr *data);

#endif /* !OBJDUMP_H_ */
